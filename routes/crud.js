async function readCrud(label, Model, req, res) {
    let founditem
    try {
        founditem = await Model.findById(req.params.id)
    } catch (error) {
        console.error(error)
        return res.json({
            success: false,
            message: `There was an error on the server trying to find requested ${label.toLowerCase()}. ${label} may not exist.`
        })
    }

    return res.json({
        success: true,
        message: `${label} has been found`,
        item: founditem
    })
}

async function createCrud(label, Model, req, res) {
    let newItem
    try {
        newItem = await Model.create(req.body)
    } catch (error) {
        console.error(error)
        return res.json({
            success: false,
            message: `There was an error on the server trying to create a new ${label.toLowerCase()}.`
        })
    }

    res.json({
        success: true,
        message: `New ${label.toLowerCase()} has been created.`,
        newItem
    })
}

async function updateCrud(label, Model, req, res) {
    let updatedItem
    try {
        updatedItem = await Model.findByIdAndUpdate(req.params.id, req.body, { new: true })
    } catch (error) {
        console.error(error)
        return res.json({
            success: false,
            message: `There was an error on the server trying to find user. ${label} may not exist.`
        })
    }

    res.json({
        success: true,
        message: `${label} has been updated.`,
        updatedItem
    })
}

async function deleteCrud(label, Model, req, res) {

    try {
        await Model.findByIdAndDelete(req.params.id, { useFindAndModify: false })
    } catch (error) {
        console.error(error)
        return res.json({
            success: false,
            message: `There was an error on the server trying to delete a ${label.toLowerCase()}.`
        })
    }


    res.json({
        success: true,
        message: `${label} has been deleted.`,
    })
}


module.exports = {
    createCrud,
    readCrud,
    updateCrud,
    deleteCrud
}