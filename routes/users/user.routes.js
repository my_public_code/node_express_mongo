const express = require('express')
const router = express.Router()

const User = require('./user.model')
const { Model } = require('mongoose')

const { createCrud, readCrud, updateCrud, deleteCrud } = require('../crud')


// Index Route - Get all users
router.get('/', async (req, res) => {
    const users = await User.find({})
    res.json({ users })
})

// Create Route
router.post('/', async (req, res) => createCrud(`User`, User, req, res))

// Read Route
router.get('/:id', async (req, res) => readCrud(`User`, User, req, res))

// Update Route
router.put('/:id', async (req, res) => updateCrud(`User`, User, req, res))


// Delete Route
router.delete('/:id', async (req, res) => deleteCrud(`User`, User, req, res))


module.exports = router