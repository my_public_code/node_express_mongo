const express = require('express')
const app = express()

// Connect to db
require('./config/db.js')

// body parsing
app.use(express.urlencoded({ extended: false }))

// parse application/json
app.use(express.json())



// Middleware === //
app.use((req, res, next) => {
    console.log(`${req.method}: ${req.url}`)
    next()
});

// Routes === //
app.use('/api', require('./routes'))

// Listen === //
const HOST = process.env.HOST || 'localhost'
const PORT = process.env.PORT || 8080
app.listen(PORT, HOST, () => console.log(`Server is listening on ${HOST}:${PORT}`))