# Node / Express / MongoDB boilerplate API

This is a work in progress. The goal is to have a truly simple REST API that can be used to start any project.

### Requirements

* Node.js
* MongoDB Database (Can connect to MongoDB Atlas)
* nodemon global

## Get Started

### Connect a database

Create a `.env` file in the root directory. Create a variable called `MONGO_CONNECT` and set it to your MongoDB connection URL.


### Install dependencies and run app

From the root of the api directory run `npm i` or `yarn` to install node packages.

Run `npm i nodemon -g` to install nodemon globally.

After packages have been downloaded, run `yarn dev` or `npm run dev` to start development server.

You should see a success message with the host and port that app is listening on printed out to the console.

